import * as repo from '../repositories/users';
import { Users } from '../entities/users';
import { IUserRequest } from '../interfaces/user';
import * as joi from 'joi';
import * as util from 'util';

import * as crypto from 'crypto';
import * as jsonwebtoken from 'jsonwebtoken';
import { ISessionRequest } from '../interfaces/session';
import { Context } from 'koa';

export const insertUser = async (user: IUserRequest) => {
    await joi.validate(user, {
        name: joi.string().required(),
        email: joi.string().required(),
        password: joi.string().required()
    });
    const insertUser = new Users();
    insertUser.name = user.name;
    insertUser.email = user.email;
    insertUser.password = generateHash(user.password);
    return repo.insert(insertUser);
};

export const session = async (ctx: Context, session: ISessionRequest) => {
    await joi.validate(session, {
        email: joi.string().required(),
        password: joi.string().required()
    });

    const sessionUser = new Users();
    sessionUser.email = session.email;
    sessionUser.password = generateHash(session.password);

    const result = await repo.getUser(sessionUser);

    if (!result) {
        ctx.throw(401, "Invalid Username or Password");
    }

    return jsonwebtoken
        .sign({
            data: session.email,
            expiresIn: '1h'
        }, 'shared-secret');

};

const generateHash = (password: any) => {
    return crypto.pbkdf2Sync(password, 'salt', 1000, 64, `sha512`).toString(`hex`);
};
