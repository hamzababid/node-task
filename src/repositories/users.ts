import { getRepository } from 'typeorm';
import { Users } from '../entities/users';

export const insert = async (user: Users) => {
    return getRepository(Users).insert(user);
};

export const getUser = async (user: Users) => {
    return getRepository(Users).findOne({ email: user.email, password: user.password });
};
