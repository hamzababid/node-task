import * as service from '../services/users';
import { Context } from 'koa';
import { IUserRequest } from '../interfaces/user';
import { ISessionRequest } from '../interfaces/session';

export const insertUser = async (ctx: Context, next: () => void) => {
    const user: IUserRequest = ctx.request.body;
    ctx.state.data = await service.insertUser(user);
    await next();
};

export const session = async (ctx: Context, next: () => void) => {
    const user: IUserRequest = ctx.request.body;
    ctx.state.data = await service.session(ctx, user);
    await next();
};